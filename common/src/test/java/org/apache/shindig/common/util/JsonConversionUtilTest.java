package org.apache.shindig.common.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Test for conversion of a structured key-value set to a JSON object
 */
public class JsonConversionUtilTest {

  public JsonConversionUtilTest() {
  }

  @Test
  public void testSimplePathToJsonParsing()
      throws Exception {
    JSONObject root = new JSONObject();
    JsonConversionUtil.buildHolder(root, "a.a.a".split("\\."), 0);
    assertJsonEquals(root, new JSONObject("{a:{a:{}}}"));
  }

  @Test
  public void testArrayPathToJsonParsing()
      throws Exception {
    JSONObject root = new JSONObject();
    JsonConversionUtil.buildHolder(root, "a.a(0).a".split("\\."), 0);
    JsonConversionUtil.buildHolder(root, "a.a(1).a".split("\\."), 0);
    JsonConversionUtil.buildHolder(root, "a.a(2).a".split("\\."), 0);
    assertJsonEquals(root, new JSONObject("{a:{a:[{},{},{}]}}"));
  }

  @Test
  public void testValueToJsonParsing()
      throws Exception {
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("abc"), "abc");
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("\"a,b,c\""), "a,b,c");
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("true"), true);
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("false"), false);
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("null"), JSONObject.NULL);
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("'abc'"), "abc");
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("a,b,c"),
        new JSONArray(Lists.newArrayList("a", "b", "c")));
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("1,2,3,true,false,null"),
        new JSONArray(Lists.newArrayList(1, 2, 3, true,
            false, null)));
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("(1)"),
        new JSONArray(Lists.newArrayList(1)));
    assertJsonEquals(JsonConversionUtil.convertToJsonValue("(true)"),
        new JSONArray(Lists.newArrayList(true)));
  }

  @Test
  public void testParameterMapToJsonParsing()
      throws Exception {
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(ImmutableMap.of("a.b.c", "1")),
        new JSONObject("{a:{b:{c:1}}}"));
    assertJsonEquals(
        JsonConversionUtil.parametersToJsonObject(ImmutableMap.of("a.b.c", "\"1\"")),
        new JSONObject("{a:{b:{c:\"1\"}}}"));
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(ImmutableMap.of("a.b.c", "true")),
        new JSONObject("{a:{b:{c:true}}}"));
    assertJsonEquals(
        JsonConversionUtil.parametersToJsonObject(ImmutableMap.of("a.b.c", "false")),
        new JSONObject("{a:{b:{c:false}}}"));
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(ImmutableMap.of("a.b.c", "null")),
        new JSONObject("{a:{b:{c:null}}}"));
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(
        ImmutableMap.of("a.b(0).c", "hello", "a.b(1).c", "hello")),
        new JSONObject("{a:{b:[{c:\"hello\"},{c:\"hello\"}]}}"));
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(
        ImmutableMap.of("a.b.c", "hello, true, false, null, 1,2, \"null\", \"()\"")),
        new JSONObject("{a:{b:{c:[\"hello\",true,false,null,1,2,\"null\",\"()\"]}}}"));
    assertJsonEquals(JsonConversionUtil.parametersToJsonObject(
        ImmutableMap.of("a.b.c", "\"hello, true, false, null, 1,2\"")),
        new JSONObject("{a:{b:{c:\"hello, true, false, null, 1,2\"}}}"));
  }

  @Test
  public void testJSONToParameterMapParsing() throws Exception {
    JsonConversionUtil.fromJson(new JSONObject("{a:{b:[{c:\"hello\"},{c:\"hello\"}]}}"));
  }

  private void assertJsonEquals(Object expected, Object actual)
      throws JSONException {
    if (expected == null) {
      assertNull(actual);
      return;
    }
    assertNotNull(actual);
    if (expected instanceof JSONObject) {
      JSONObject expectedObject = (JSONObject) expected;
      JSONObject actualObject = (JSONObject) actual;
      if (expectedObject.length() == 0) {
        assertEquals(expectedObject.length(), actualObject.length());
        return;
      }
      assertEquals(expectedObject.names().length(), actualObject.names().length());
      String key;
      for (Iterator keys = expectedObject.keys(); keys.hasNext();
          assertJsonEquals(expectedObject.get(key), actualObject.get(key))) {
        key = (String) keys.next();
        assertTrue(actualObject.has(key));
      }
    } else if (expected instanceof JSONArray) {
      JSONArray expectedArray = (JSONArray) expected;
      JSONArray actualArray = (JSONArray) actual;
      assertEquals(expectedArray.length(), actualArray.length());
      for (int i = 0; i < expectedArray.length(); i++) {
        if (expectedArray.isNull(i)) {
          assertTrue(actualArray.isNull(i));
        } else {
          assertJsonEquals(expectedArray.get(i), actualArray.get(i));
        }
      }

    } else {
      assertEquals(expected, actual);
    }
  }
}
