# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.2.0] - 2019-07-22
### Added
- Java 11 compatibility.

### Changed
- Bumped Guava, Guice, and XStream to Java 11 compatible versions.
- Now compiling against the Servlet 3.0.1 API.
- Flattened the project structure to one parent POM with four modules.
- Moved several resource files to remove the need for custom resource locations in POM.

### Removed
- Modules not required by Atlassian products (e.g. Java assembly, Java samples, Java server, PHP)

[Unreleased]: https://bitbucket.org/atlassian/apache-shindig/src
[2.2.0]: https://bitbucket.org/atlassian/apache-shindig/commits/d0eb368d327aab5d5d356c9e72bb3261133c2e2e
