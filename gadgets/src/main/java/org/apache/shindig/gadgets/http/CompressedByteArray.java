/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.gadgets.http;

import java.io.IOException;

import org.xerial.snappy.Snappy;

import static java.lang.Boolean.getBoolean;

/**
 * Compressed array of bytes which uncompresses it each times when original array is needed. In addition
 * maintains original array length because it's needed to put correct content length up the stack.
 * <p>
 * Compression is enabled only when system property is ON. While it might look like it decreases performance in fact
 * compression for megabytes of data measure in microseconds and could easily justified by decrease in GC times.
 */
class CompressedByteArray
{
    static final String GADGETS_COMPRESSION_ENABLED_KEY = "gadgets.compression.enabled";
    static final boolean GADGETS_COMPRESSION_ENABLED_VALUE = getBoolean(GADGETS_COMPRESSION_ENABLED_KEY);

    private final byte[] bytes;
    private final int length;

    public CompressedByteArray(byte[] bytes)
    {
        if (GADGETS_COMPRESSION_ENABLED_VALUE)
        {
            this.bytes = bytes != null
                    ? zip(bytes)
                    : null;
        }
        else
        {
            this.bytes = bytes;
        }

        this.length = bytes != null
                ? bytes.length
                : 0;
    }

    byte[] compressedBytes()
    {
        return bytes;
    }

    byte[] uncompressedBytes()
    {
        if (GADGETS_COMPRESSION_ENABLED_VALUE)
        {
            return bytes != null
                    ? unzip(bytes)
                    : null;
        }
        else
        {
            return bytes;
        }
    }

    int length()
    {
        return length;
    }

    private static byte[] zip(byte[] bytes)
    {
        try
        {
            return Snappy.compress(bytes);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static byte[] unzip(byte[] bytes)
    {
        try
        {
            return Snappy.uncompress(bytes);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}


