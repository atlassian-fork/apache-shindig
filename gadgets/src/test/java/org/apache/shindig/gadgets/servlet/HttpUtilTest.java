/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import com.google.common.collect.ImmutableSet;
import org.apache.shindig.gadgets.GadgetContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.Set;

import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HttpUtilTest extends ServletTestFixture {
  private final static String CONTAINER = "container";
  private final static String FEATURE_0 = "featureZero";
  private final static String FEATURE_1 = "feature-One";

  private final GadgetContext context = mock(GadgetContext.class);

  @Test
  public void testSetCachingHeaders() {
    HttpUtil.setCachingHeaders(recorder);

    checkCacheControlHeaders(HttpUtil.DEFAULT_TTL, false);
  }

  @Test
  public void testSetCachingHeadersNoProxy() {
    HttpUtil.setCachingHeaders(recorder, true);

    checkCacheControlHeaders(HttpUtil.DEFAULT_TTL, true);
  }

  @Test
  public void testSetCachingHeadersAllowProxy() {
    HttpUtil.setCachingHeaders(recorder, false);

    checkCacheControlHeaders(HttpUtil.DEFAULT_TTL, false);
  }

  @Test
  public void testSetCachingHeadersFixedTtl() {
    int ttl = 10;
    HttpUtil.setCachingHeaders(recorder, ttl);

    checkCacheControlHeaders(ttl, false);
  }

  @Test
  public void testSetCachingHeadersWithTtlAndNoProxy() {
    int ttl = 20;
    HttpUtil.setCachingHeaders(recorder, ttl, true);

    checkCacheControlHeaders(ttl, true);
  }

  @Test
  public void testSetCachingHeadersNoCache() {
    HttpUtil.setCachingHeaders(recorder, 0);

    checkCacheControlHeaders(0, true);
  }

  private void assertJsonEquals(JSONObject lhs, JSONObject rhs) throws JSONException {
    for (String key : JSONObject.getNames(lhs)) {
      Object obj = lhs.get(key);
      if (obj instanceof String) {
        assertEquals(obj, rhs.get(key));
      } else if (obj instanceof JSONObject) {
        assertJsonEquals((JSONObject)obj, rhs.getJSONObject(key));
      } else {
        fail("Unsupported type: " + obj.getClass());
      }
    }
  }

  @Test
  public void testGetJsConfig() throws JSONException {
    JSONObject features = new JSONObject()
        .put(FEATURE_0, "config")
        .put(FEATURE_1, "other config");

    Set<String> needed = ImmutableSet.of(FEATURE_0, FEATURE_1);

    when(context.getContainer()).thenReturn(CONTAINER);
    when(containerConfig.getJsonObject(CONTAINER, "gadgets.features"))
        .thenReturn(features);

    assertJsonEquals(features, HttpUtil.getJsConfig(containerConfig, context, needed));
  }

  @Test
  public void testGetJsConfigNoFeatures() {
    when(context.getContainer()).thenReturn(CONTAINER);
    when(containerConfig.getJsonObject(CONTAINER, "gadgets.features"))
        .thenReturn(null);

    JSONObject results = HttpUtil.getJsConfig(containerConfig, context, emptySet());

    assertEquals("Results should be empty when there are no features", 0, results.length());
  }
}