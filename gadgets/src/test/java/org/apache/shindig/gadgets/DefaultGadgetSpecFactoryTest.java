/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets;

import org.apache.shindig.common.cache.CacheProvider;
import org.apache.shindig.common.cache.LruCacheProvider;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.http.HttpFetcher;
import org.apache.shindig.gadgets.http.HttpRequest;
import org.apache.shindig.gadgets.http.HttpResponse;
import org.apache.shindig.gadgets.http.HttpResponseBuilder;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.net.URI;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for DefaultGadgetSpecFactory
 */
public class DefaultGadgetSpecFactoryTest {
  private final static Uri SPEC_URL = Uri.parse("http://example.org/gadget.xml");
  private final static Uri REMOTE_URL = Uri.parse("http://example.org/remote.html");
  private final static String LOCAL_CONTENT = "Hello, local content!";
  private final static String ALT_LOCAL_CONTENT = "Hello, local content!";
  private final static String RAWXML_CONTENT = "Hello, rawxml content!";
  private final static String LOCAL_SPEC_XML
      = "<Module>" +
        "  <ModulePrefs title='GadgetSpecFactoryTest'/>" +
        "  <Content type='html'>" + LOCAL_CONTENT + "</Content>" +
        "</Module>";
  private final static String ALT_LOCAL_SPEC_XML
      = "<Module>" +
        "  <ModulePrefs title='GadgetSpecFactoryTest'/>" +
        "  <Content type='html'>" + ALT_LOCAL_CONTENT + "</Content>" +
        "</Module>";
  private final static String RAWXML_SPEC_XML
      = "<Module>" +
        "  <ModulePrefs title='GadgetSpecFactoryTest'/>" +
        "  <Content type='html'>" + RAWXML_CONTENT + "</Content>" +
        "</Module>";
  private final static String URL_SPEC_XML
      = "<Module>" +
        "  <ModulePrefs title='GadgetSpecFactoryTest'/>" +
        "  <Content type='url' href='" + REMOTE_URL + "'/>" +
        "</Module>";

  private final static GadgetContext NO_CACHE_CONTEXT = new GadgetContext() {
    @Override
    public boolean getIgnoreCache() {
      return true;
    }
    @Override
    public URI getUrl() {
      return SPEC_URL.toJavaUri();
    }
  };

  private final static GadgetContext RAWXML_GADGET_CONTEXT = new GadgetContext() {
    @Override
    public boolean getIgnoreCache() {
      // This should be ignored by calling code.
      return false;
    }

    @Override
    public URI getUrl() {
      return SPEC_URL.toJavaUri();
    }

    @Override
    public String getParameter(String param) {
      if (param.equals(DefaultGadgetSpecFactory.RAW_GADGETSPEC_XML_PARAM_NAME)) {
        return RAWXML_SPEC_XML;
      }
      return null;
    }
  };

  private static final int MAX_AGE = 10000;

  private final HttpFetcher fetcher = mock(HttpFetcher.class);

  private final CacheProvider cacheProvider = new LruCacheProvider(5);

  private final DefaultGadgetSpecFactory specFactory
      = new DefaultGadgetSpecFactory(fetcher, cacheProvider, MAX_AGE);

  @Test
  public void specFetched() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    HttpResponse response = new HttpResponse(LOCAL_SPEC_XML);
    when(fetcher.fetch(request)).thenReturn(response);

    GadgetSpec spec = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);

    assertEquals(LOCAL_CONTENT, spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
  }

  @Test
  public void specFetchedWithContext() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    HttpResponse response = new HttpResponse(LOCAL_SPEC_XML);
    when(fetcher.fetch(request)).thenReturn(response);

    GadgetSpec spec = specFactory.getGadgetSpec(NO_CACHE_CONTEXT);

    assertEquals(LOCAL_CONTENT, spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
  }

  @Test
  public void specFetchedFromParam() throws Exception {
    // Set up request as if it's a regular spec request, and ensure that
    // the return value comes from rawxml, not the fetcher.
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(false);
    HttpResponse response = new HttpResponse(LOCAL_SPEC_XML);
    when(fetcher.fetch(request)).thenReturn(response);

    GadgetSpec spec = specFactory.getGadgetSpec(RAWXML_GADGET_CONTEXT);

    assertEquals(RAWXML_CONTENT, spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
    assertEquals(DefaultGadgetSpecFactory.RAW_GADGET_URI, spec.getUrl());
  }

  @Test
  public void staleSpecIsRefetched() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    HttpRequest retriedRequest = new HttpRequest(SPEC_URL).setIgnoreCache(false);
    HttpResponse expiredResponse = new HttpResponseBuilder()
        .addHeader("Pragma", "no-cache")
        .setResponse(LOCAL_SPEC_XML.getBytes("UTF-8"))
        .create();
    HttpResponse updatedResponse = new HttpResponse(ALT_LOCAL_SPEC_XML);
    when(fetcher.fetch(request)).thenReturn(expiredResponse);
    when(fetcher.fetch(retriedRequest)).thenReturn(updatedResponse);

    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);
    GadgetSpec spec = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);

    assertEquals(ALT_LOCAL_CONTENT, spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
  }

  @Test
  public void staleSpecReturnedFromCacheOnError() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    HttpRequest retriedRequest = new HttpRequest(SPEC_URL).setIgnoreCache(false);
    HttpResponse expiredResponse = new HttpResponseBuilder()
        .setResponse(LOCAL_SPEC_XML.getBytes("UTF-8"))
        .addHeader("Pragma", "no-cache")
        .create();
    when(fetcher.fetch(request)).thenReturn(expiredResponse);
    when(fetcher.fetch(retriedRequest)).thenReturn(HttpResponse.notFound());

    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);
    GadgetSpec spec = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);

    assertEquals(ALT_LOCAL_CONTENT, spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
  }

  @Test
  public void ttlPropagatesToFetcher() throws Exception {
    CapturingFetcher capturingFetcher = new CapturingFetcher();

    DefaultGadgetSpecFactory forcedCacheFactory
        = new DefaultGadgetSpecFactory(capturingFetcher, cacheProvider, 10000);

    forcedCacheFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);

    assertEquals(10, capturingFetcher.request.getCacheTtl());
  }

  @Test
  public void typeUrlNotFetchedRemote() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    HttpResponse response = new HttpResponse(URL_SPEC_XML);
    when(fetcher.fetch(request)).thenReturn(response);

    GadgetSpec spec = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);

    assertEquals(REMOTE_URL, spec.getView(GadgetSpec.DEFAULT_VIEW).getHref());
    assertEquals("", spec.getView(GadgetSpec.DEFAULT_VIEW).getContent());
  }

  @Test(expected = GadgetException.class)
  public void badFetchThrows() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    when(fetcher.fetch(request)).thenReturn(HttpResponse.error());

    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);
  }

  @Test(expected = GadgetException.class)
  public void badFetchServesCached() throws Exception {
    HttpRequest firstRequest = new HttpRequest(SPEC_URL).setIgnoreCache(false);
    when(fetcher.fetch(firstRequest)).thenReturn(new HttpResponse(LOCAL_SPEC_XML));
    HttpRequest secondRequest = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    when(fetcher.fetch(secondRequest)).thenReturn(HttpResponse.error());

    GadgetSpec original = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);
    GadgetSpec cached = specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);

    assertEquals(original.getUrl(), cached.getUrl());
    assertEquals(original.getChecksum(), cached.getChecksum());
  }

  @Test(expected = GadgetException.class)
  public void malformedGadgetSpecThrows() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    when(fetcher.fetch(request)).thenReturn(new HttpResponse("malformed junk"));

    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);
  }

  @Test(expected = GadgetException.class)
  public void malformedGadgetSpecIsCachedAndThrows() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(false);
    when(fetcher.fetch(request)).thenReturn(new HttpResponse("malformed junk"));

    try {
      specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);
      fail("No exception thrown on bad parse");
    } catch (GadgetException e) {
      // Expected.
    }
    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);
  }

  @Test(expected = GadgetException.class)
  public void throwingFetcherRethrows() throws Exception {
    HttpRequest request = new HttpRequest(SPEC_URL).setIgnoreCache(true);
    when(fetcher.fetch(request)).thenThrow(
        new GadgetException(GadgetException.Code.FAILED_TO_RETRIEVE_CONTENT));

    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);
  }

  /**
   * When we sent two requests in the beginning (with a cold cache), we expect that both of them will be sent
   */
  @Test
  public void twoRequestsAreSentToGadgetSpecProviderInParallelIfCacheIsEmpty() throws GadgetException, ExecutionException, InterruptedException {
    final int REQUESTS_IN_PARALLEL = 2;
    final CyclicBarrier barrier = new CyclicBarrier(REQUESTS_IN_PARALLEL);

    final ExecutorService executor = Executors.newCachedThreadPool();

    when(fetcher.fetch(any())).thenAnswer((Answer<HttpResponse>) invocation -> {
      // We check that fetcher.fetch was called for both requests
      barrier.await(1, TimeUnit.SECONDS);
      return new HttpResponse(LOCAL_SPEC_XML);
    });

    Future<GadgetSpec> thread1 = executor.submit(() -> specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false));
    Future<GadgetSpec> thread2 = executor.submit(() -> specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false));

    assertEquals(SPEC_URL, thread1.get().getUrl());
    assertEquals(SPEC_URL, thread2.get().getUrl());
  }

  /**
   * When we have an expired value in the cache, and two threads are trying to retrieve it,
   * only one request will be sent to the gadget provider
   */
  @Test
  public void onlyOneRequestIsSentToSpecProviderIfCacheIsNotEmpty() throws GadgetException, ExecutionException, InterruptedException {
    // Part 1: warm the cache
    when(fetcher.fetch(any())).thenReturn((new HttpResponse(LOCAL_SPEC_XML)));

    // Note that the refresh interval is 0, it means that the records are expired just after adding to the cache
    DefaultGadgetSpecFactory specFactory
        = new DefaultGadgetSpecFactory(fetcher, cacheProvider, 0);
    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), true);

    // Part 2: send two requests at the same time from two threads
    // and check that the http fetcher was called only for one of the threads
    final ExecutorService executor = Executors.newCachedThreadPool();

    when(fetcher.fetch(any())).thenAnswer((Answer<HttpResponse>) invocation -> {
      // This is called when the first worker tries to get the gadget spec
      // So we run the second worker from here. We do not expect that the second worker will call this method
      executor.submit(() -> specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false)).get();
      verify(fetcher, times(2)).fetch(any());
      return new HttpResponse(LOCAL_SPEC_XML);
    });

    // Run the first worker and wait for the results
    Future<GadgetSpec> thread1 = executor.submit(() -> specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false));
    assertEquals(SPEC_URL, thread1.get().getUrl());

    // Now, we know that the first call was done in the beginning (when we warmed up the cache)
    // and the second request was done by one of the workers (the second worker hasn't sent a request to the gadget spec provider)
    verify(fetcher, times(2)).fetch(any());
  }

  /**
   * When we have non-expired record in the cache, we are retrieving gadget spec from the cache
   * and we do not expect any external calls
   */
  @Test
  public void requestsAreNotSentToSpecProviderIfCacheIsNotEmpty() throws GadgetException, ExecutionException, InterruptedException {
    // init cache
    when(fetcher.fetch(any())).thenReturn((new HttpResponse(LOCAL_SPEC_XML)));
    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);
    verify(fetcher, times(1)).fetch(any());

    // send the second request
    specFactory.getGadgetSpec(SPEC_URL.toJavaUri(), false);

    // verify that no more requests were sent
    verify(fetcher, times(1)).fetch(any());
  }

  private static class CapturingFetcher implements HttpFetcher {
    HttpRequest request;

    public HttpResponse fetch(HttpRequest request) {
      this.request = request;
      return new HttpResponse(LOCAL_SPEC_XML);
    }
  }
}
