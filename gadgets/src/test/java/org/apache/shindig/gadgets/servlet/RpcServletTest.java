/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for RpcServlet.
 */
public class RpcServletTest {
  private RpcServlet servlet;
  private JsonRpcHandler handler;
  
  @Before
  public void setUp() {
    servlet = new RpcServlet();
    handler = mock(JsonRpcHandler.class);
    servlet.setJsonRpcHandler(handler);
  }

  @Test
  public void testDoGetNormal() throws Exception {
    HttpServletRequest request = createGetRequest("{\"gadgets\":[]}",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._");
    HttpServletResponse response = createHttpResponse("Content-Disposition",
        "attachment;filename=rpc.txt", "application/json; charset=utf-8",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._({\"GADGETS\":[]})",
        HttpServletResponse.SC_OK);
    JSONObject handlerResponse = new JSONObject("{\"GADGETS\":[]}");
    when(handler.process(isA(JSONObject.class))).thenReturn(handlerResponse);

    servlet.doGet(request, response);
  }

  @Test
  public void testDoGetWithHandlerRpcException() throws Exception {
    HttpServletRequest request = createGetRequest("{\"gadgets\":[]}", "function");
    HttpServletResponse response = createHttpResponse("rpcExceptionMessage",
        HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    when(handler.process(isA(JSONObject.class))).thenThrow(new RpcException("rpcExceptionMessage"));

    servlet.doGet(request, response);
  }

  @Test
  public void testDoGetWithHandlerJsonException() throws Exception {
    HttpServletRequest request = createGetRequest("{\"gadgets\":[]}", "function");
    HttpServletResponse response = createHttpResponse("Malformed JSON request.",
        HttpServletResponse.SC_BAD_REQUEST);
    when(handler.process(isA(JSONObject.class))).thenThrow(new JSONException("json"));

    servlet.doGet(request, response);
  }

  @Test
  public void testDoGetWithMissingReqParam() throws Exception {
    HttpServletRequest request = createGetRequest(null, "function");
    HttpServletResponse response = createHttpResponse(null, HttpServletResponse.SC_BAD_REQUEST);

    servlet.doGet(request, response);
  }

  @Test
  public void testDoGetWithMissingCallbackParam() throws Exception {
    HttpServletRequest request = createGetRequest("{\"gadgets\":[]}", null);
    HttpServletResponse response = createHttpResponse(null, HttpServletResponse.SC_BAD_REQUEST);

    servlet.doGet(request, response);
  }

  @Test
  public void testDoGetWithBadCallbackParamValue() throws Exception {
    HttpServletRequest request = createGetRequest("{\"gadgets\":[]}", "/'!=");
    HttpServletResponse response = createHttpResponse(null, HttpServletResponse.SC_BAD_REQUEST);

    servlet.doGet(request, response);
  }

  private HttpServletRequest createGetRequest(String reqParamValue, String callbackParamValue) {
    HttpServletRequest result = mock(HttpServletRequest.class);
    when(result.getMethod()).thenReturn("GET");
    when(result.getCharacterEncoding()).thenReturn("UTF-8");
    when(result.getParameter(RpcServlet.GET_REQUEST_REQ_PARAM)).thenReturn(reqParamValue);
    when(result.getParameter(RpcServlet.GET_REQUEST_CALLBACK_PARAM)).thenReturn(callbackParamValue);
    return result;
  }

  private HttpServletResponse createHttpResponse(String response, int httpStatusCode)
    throws IOException {
    return createHttpResponse(null, null, null, response, httpStatusCode);
  }

  private HttpServletResponse createHttpResponse(String header1, String header2,
      String contentType, String response, int httpStatusCode) throws IOException {
    HttpServletResponse result = mock(HttpServletResponse.class);
    PrintWriter writer = mock(PrintWriter.class);
    if (response != null) {
      when(result.getWriter()).thenReturn(writer);
      writer.write(response);
    }
    if (header1 != null && header2 != null) {
      result.setHeader(header1, header2);
    }
    if (contentType != null) {
      result.setContentType(contentType);
    }
    result.setStatus(httpStatusCode);
    return result;
  }

}
