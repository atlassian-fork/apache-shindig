/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

public class ProxyServletRequestTest {
  private final static String URL = "http://proxy/url";

  private final HttpServletRequest request = mock(HttpServletRequest.class);

  public ProxyServletRequest setupMockRequest(String url) {
    when(request.getRequestURI()).thenReturn(url);
    when(request.getParameter("url")).thenReturn(URL);
    return new ProxyServletRequest(request);
  }

  @Test
  public void testOldRequestSyntax() {
    ProxyServletRequest req = setupMockRequest(
      "http://localhost/gadgets/proxy?url=" + URL
    );
    Assert.assertFalse(req.isUsingChainedSyntax());
    assertEquals(URL, req.getParameter("url"));
  }

  @Test
  public void testChainedSyntaxWithNoParameters() {
    ProxyServletRequest req = setupMockRequest(
      "http://localhost/gadgets/proxy//http://remote/proxy?query=foo"
    );
    assertTrue(req.isUsingChainedSyntax());
    assertEquals("http://remote/proxy?query=foo", req.getParameter("url"));
    assertNull(req.getParameter("query"));
  }

  @Test
  public void testChainedSyntaxWithOneParameter() {
    ProxyServletRequest req = setupMockRequest(
      "http://localhost/gadgets/proxy/nocache=1/http://remote/proxy?nocache=0"
    );
    assertTrue(req.isUsingChainedSyntax());
    assertEquals("http://remote/proxy?nocache=0", req.getParameter("url"));
    assertEquals("1", req.getParameter("nocache"));
  }

  @Test
  public void testChainedSyntaxWithParameters() {
    ProxyServletRequest req = setupMockRequest(
      "http://u:p@127.0.0.1:80/g/proxy/a=b%20+c&url=u/http://r/p?a=d+e"
    );
    assertTrue(req.isUsingChainedSyntax());
    assertEquals("http://r/p?a=d+e", req.getParameter("url"));
    assertEquals("b  c", req.getParameter("a"));
  }
}

