package org.apache.shindig.gadgets.rewrite;

import org.apache.commons.io.IOUtils;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.http.HttpRequest;
import org.apache.shindig.gadgets.http.HttpResponse;
import org.apache.shindig.gadgets.http.HttpResponseBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CSSContentRewriterTest extends BaseRewriterTestCase {

  private CSSContentRewriter rewriter;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    ContentRewriterFeature overrideFeature =
        rewriterFeatureFactory.get(createSpecWithRewrite(".*", ".*exclude.*", "HTTP",
            HTMLContentRewriter.TAGS));
    ContentRewriterFeatureFactory factory = mockContentRewriterFeatureFactory(overrideFeature);
    rewriter = new CSSContentRewriter(factory, DEFAULT_PROXY_BASE);
  }

  @Test
  public void testCssBasic() throws Exception {
    String content = readFile("org/apache/shindig/gadgets/rewrite/rewritebasic.css");
    String expected = readFile("org/apache/shindig/gadgets/rewrite/rewritebasic-expected.css");
    HttpRequest request = new HttpRequest(Uri.parse("http://www.example.org/path/rewritebasic.css"));
    request.setMethod("GET");
    request.setGadget(SPEC_URL);

    HttpResponse response = new HttpResponseBuilder().setHeader("Content-Type", "text/css")
      .setResponseString(content).create();

    MutableContent mc = new MutableContent(null, content);
    rewriter.rewrite(request, response, mc);

    assertEquals(expected, mc.getContent());
  }

  @Test
  public void testNoRewriteUnknownMimeType() {
    // Strict mock as we expect no calls
    MutableContent mc = mock(MutableContent.class);
    HttpRequest req = mock(HttpRequest.class);
    when(req.getRewriteMimeType()).thenReturn("unknown");

    assertNull(rewriter.rewrite(req, fakeResponse, mc));
  }

  private String readFile(final String relativePath) throws IOException {
    final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(relativePath);
    assertNotNull("No such file '" + relativePath + "'", inputStream);
    return IOUtils.toString(inputStream, UTF_8);
  }
}
