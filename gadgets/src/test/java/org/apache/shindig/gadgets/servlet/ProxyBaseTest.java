/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import com.google.common.collect.Maps;
import org.apache.shindig.common.ContainerConfig;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.GadgetException;
import org.apache.shindig.gadgets.http.HttpResponse;
import org.apache.shindig.gadgets.http.HttpResponseBuilder;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests for ProxyBase.
 */
public class ProxyBaseTest extends ServletTestFixture {

  private final ProxyBase proxy = new ProxyBase() {
    @Override
    public void fetch(HttpServletRequest request, HttpServletResponse response) {
      // Nothing.
    }
  };

  @Test
  public void testValidateUrlNoPath() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/", url.getPath());
    assertNull(url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlHttps() throws Exception {
    Uri url = proxy.validateUrl("https://www.example.com");
    assertEquals("https", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/", url.getPath());
    assertNull(url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlWithPath() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com/foo");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/foo", url.getPath());
    assertNull(url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlWithPort() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com:8080/foo");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com:8080", url.getAuthority());
    assertEquals("/foo", url.getPath());
    assertNull(url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlWithEncodedPath() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com/foo%20bar");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/foo%20bar", url.getPath());
    assertNull(url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlWithEncodedQuery() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com/foo?q=with%20space");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/foo", url.getPath());
    assertEquals("q=with%20space", url.getQuery());
    assertEquals("with space", url.getQueryParameter("q"));
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlWithNoPathAndEncodedQuery() throws Exception {
    Uri url = proxy.validateUrl("http://www.example.com?q=with%20space");
    assertEquals("http", url.getScheme());
    assertEquals("www.example.com", url.getAuthority());
    assertEquals("/", url.getPath());
    assertEquals("q=with%20space", url.getQuery());
    assertNull(url.getFragment());
  }

  @Test
  public void testValidateUrlNullInput() {
    try {
      proxy.validateUrl(null);
      fail("Should have thrown");
    } catch (GadgetException e) {
      // good
    }
  }

  @Test
  public void testValidateUrlBadInput() {
    try {
    proxy.validateUrl("%$#%#$%#$%");
    } catch (GadgetException e) {
      // good
    }
  }

  @Test
  public void testValidateUrlBadProtocol() {
    try {
    proxy.validateUrl("gopher://foo");
  } catch (GadgetException e) {
    // good
  }
  }

  @Test
  public void testSetResponseHeaders() {
    HttpResponse results = new HttpResponseBuilder().create();

    proxy.setResponseHeaders(request, recorder, results);

    // Just verify that they were set. Specific values are configurable.
    assertNotNull("Expires header not set", recorder.getHeader("Expires"));
    assertNotNull("Cache-Control header not set", recorder.getHeader("Cache-Control"));
    assertEquals("attachment;filename=p.txt", recorder.getHeader("Content-Disposition"));
  }

  @Test
  public void testSetResponseHeadersForFlash() {
    HttpResponse results = new HttpResponseBuilder()
        .setHeader("Content-Type", "application/x-shockwave-flash")
        .create();

    proxy.setResponseHeaders(request, recorder, results);

    // Just verify that they were set. Specific values are configurable.
    assertNotNull("Expires header not set", recorder.getHeader("Expires"));
    assertNotNull("Cache-Control header not set", recorder.getHeader("Cache-Control"));
    assertNull("Content-Disposition header set for flash",
        recorder.getHeader("Content-Disposition"));
  }

  @Test
  public void testSetResponseHeadersNoCache() {
    Map<String, List<String>> headers = Maps.newTreeMap(String.CASE_INSENSITIVE_ORDER);
    headers.put("Pragma", singletonList("no-cache"));
    HttpResponse results = new HttpResponseBuilder()
        .addHeader("Pragma", "no-cache")
        .create();

    proxy.setResponseHeaders(request, recorder, results);

    // Just verify that they were set. Specific values are configurable.
    assertNotNull("Expires header not set", recorder.getHeader("Expires"));
    assertEquals("no-cache", recorder.getHeader("Pragma"));
    assertEquals("no-cache", recorder.getHeader("Cache-Control"));
    assertEquals("attachment;filename=p.txt", recorder.getHeader("Content-Disposition"));
  }

  @Test
  public void testSetResponseHeadersForceParam() {
    HttpResponse results = new HttpResponseBuilder().create();
    when(request.getParameter(ProxyBase.REFRESH_PARAM)).thenReturn("30");

    proxy.setResponseHeaders(request, recorder, results);

    checkCacheControlHeaders(30, false);
    assertEquals("attachment;filename=p.txt", recorder.getHeader("Content-Disposition"));
  }

  @Test
  public void testGetParameter() {
    when(request.getParameter("foo")).thenReturn("bar");

    assertEquals("bar", proxy.getParameter(request, "foo", "not foo"));
  }

  @Test
  public void testGetParameterWithNullValue() {
    when(request.getParameter("foo")).thenReturn(null);

    assertEquals("not foo", proxy.getParameter(request, "foo", "not foo"));
  }

  @Test
  public void testGetContainerWithContainer() {
    when(request.getParameter(ProxyBase.CONTAINER_PARAM)).thenReturn("bar");

    assertEquals("bar", proxy.getContainer(request));
  }

  @Test
  public void testGetContainerWithSynd() {
    when(request.getParameter(ProxyBase.CONTAINER_PARAM)).thenReturn(null);
    when(request.getParameter(ProxyBase.SYND_PARAM)).thenReturn("syndtainer");

    assertEquals("syndtainer", proxy.getContainer(request));
  }

  @Test
  public void testGetContainerNoParam() {
    when(request.getParameter(ProxyBase.CONTAINER_PARAM)).thenReturn(null);
    when(request.getParameter(ProxyBase.SYND_PARAM)).thenReturn(null);

    assertEquals(ContainerConfig.DEFAULT_CONTAINER, proxy.getContainer(request));
  }
}
