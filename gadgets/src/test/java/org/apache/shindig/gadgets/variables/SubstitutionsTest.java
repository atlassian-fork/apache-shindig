/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.variables;

import org.apache.shindig.gadgets.variables.Substitutions.Type;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubstitutionsTest {
  private Substitutions subst;

  @Before
  public void setUp() {
    subst = new Substitutions();
  }

  @Test
  public void testMessages() {
    String msg = "Hello, __MSG_world__!";
    subst.addSubstitution(Type.MESSAGE, "world", "planet");
    assertEquals("Hello, planet!", subst.substituteString(msg));
  }

  @Test
  public void testRandomUnderscores() {
    String msg = "Hello __root. __MSG_greetings__!";
    subst.addSubstitution(Type.MESSAGE, "greetings", "Long live the king");
    assertEquals("Hello __root. Long live the king!", subst.substituteString(msg));
  }

  @Test
  public void testLazyMatching(){
    String msg = "__MSG_key1__<div>not-a-key</div>__MSG_key2__";
    subst.addSubstitution(Type.MESSAGE, "key1", "One");
    subst.addSubstitution(Type.MESSAGE, "key2", "Two");

    assertEquals("One<div>not-a-key</div>Two", subst.substituteString(msg));
  }

  @Test
  public void testIgnoresNonPrefix(){
    String msg = "__NONPREFIX__MSG_key1__";
    subst.addSubstitution(Type.MESSAGE, "key1", "One");

    assertEquals("__NONPREFIXOne", subst.substituteString(msg));
  }

  @Test
  public void testBidi() {
    String msg = "Hello, __BIDI_DIR__-world!";
    subst.addSubstitution(Type.BIDI, "DIR", "rtl");
    assertEquals("Hello, rtl-world!", subst.substituteString(msg));
  }

  @Test
  public void testUserPref() {
    String msg = "__UP_hello__, world!";
    subst.addSubstitution(Type.USER_PREF, "hello", "Greetings");
    assertEquals("Greetings, world!", subst.substituteString(msg));
  }

  @Test
  public void testCorrectOrder() {
    String msg = "__UP_hello__, __MSG_world__!";
    subst.addSubstitution(Type.MESSAGE, "world",
        "planet __BIDI_DIR__-__UP_planet__");
    subst.addSubstitution(Type.BIDI, "DIR", "rtl");
    subst.addSubstitution(Type.USER_PREF, "hello", "Greetings");
    subst.addSubstitution(Type.USER_PREF, "planet", "Earth");
    assertEquals("Greetings, planet rtl-Earth!", subst.substituteString(msg));
  }

  @Test
  public void testIncorrectOrder() {
    String msg = "__UP_hello__, __MSG_world__";
    subst.addSubstitution(Type.MESSAGE, "world",
        "planet __MSG_earth____UP_punc__");
    subst.addSubstitution(Type.MESSAGE, "earth", "Earth");
    subst.addSubstitution(Type.USER_PREF, "punc", "???");
    subst.addSubstitution(Type.USER_PREF, "hello",
        "Greetings __MSG_foo____UP_bar__");
    subst.addSubstitution(Type.MESSAGE, "foo", "FOO!!!");
    subst.addSubstitution(Type.USER_PREF, "bar", "BAR!!!");
    assertEquals("Greetings __MSG_foo____UP_bar__, planet __MSG_earth__???",
        subst.substituteString(msg));
  }

}
