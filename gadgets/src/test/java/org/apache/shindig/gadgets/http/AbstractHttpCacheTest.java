/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.gadgets.http;

import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Sanity test to ensure that Guice injection of a default
 * ContentRewriter leads to its use by an AbstractHttpCache
 * in properly rewriting cacheable content.
 */
public class AbstractHttpCacheTest {

  @Test
  public void testCache() {
    // Setup: could move this elsewhere, but no real need right now.
    HttpCacheKey key = mock(HttpCacheKey.class);
    when(key.isCacheable()).thenReturn(true);
    HttpRequest request = mock(HttpRequest.class);
    when(request.getIgnoreCache()).thenReturn(false);
    when(request.getCacheTtl()).thenReturn(Integer.MAX_VALUE);

    HttpResponse response = new HttpResponseBuilder().setHttpStatusCode(200)
        .setResponse("foo".getBytes()).setCacheTtl(Integer.MAX_VALUE).create();

    // Actual test.
    HttpCache ahc = new TestHttpCache();
    HttpResponse added = ahc.addResponse(key, request, response);
    assertNotSame(added, response);

    // Not rewritten (anymore).
    assertEquals("foo", added.getResponseAsString());
    assertSame(added, ahc.getResponse(key, request));
    assertEquals(response, ahc.removeResponse(key));
  }

  private static class TestHttpCache extends AbstractHttpCache {
    private final Map<String, HttpResponse> map;

    public TestHttpCache() {
      super();
      map = Maps.newHashMap();
    }

    @Override
    public void addResponseImpl(String key, HttpResponse response) {
      map.put(key, response);
    }

    @Override
    public HttpResponse getResponseImpl(String key) {
      return map.get(key);
    }

    @Override
    public HttpResponse removeResponseImpl(String key) {
      return map.remove(key);
    }
  }
}
