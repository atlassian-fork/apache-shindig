/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import com.google.common.collect.Lists;
import org.apache.shindig.auth.AuthInfo;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.AuthType;
import org.apache.shindig.gadgets.GadgetException;
import org.apache.shindig.gadgets.http.HttpRequest;
import org.apache.shindig.gadgets.http.HttpResponse;
import org.apache.shindig.gadgets.http.HttpResponseBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static junitx.framework.StringAssert.assertStartsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests for MakeRequestHandler.
 */
public class MakeRequestHandlerTest extends ServletTestFixture {
  private static final Uri REQUEST_URL = Uri.parse("http://example.org/file");
  private static final String REQUEST_BODY = "I+am+the+request+body!foo=baz%20la";
  private static final String RESPONSE_BODY = "makeRequest response body";
  private static final SecurityToken DUMMY_TOKEN = new FakeGadgetToken();

  private final MakeRequestHandler handler
      = new MakeRequestHandler(fetcherFactory, rewriterRegistry);

  private void expectGetAndReturnBody(String response) throws Exception {
    expectGetAndReturnBody(AuthType.NONE, response);
  }

  private void expectGetAndReturnBody(AuthType authType, String response) throws Exception {
    HttpRequest request = new HttpRequest(REQUEST_URL).setAuthType(authType);
    when(fetcherFactory.fetch(request)).thenReturn(new HttpResponse(response));
  }

  private void expectPostAndReturnBody(String postData, String response) throws Exception {
    expectPostAndReturnBody(AuthType.NONE, postData, response);
  }

  private void expectPostAndReturnBody(AuthType authType, String postData, String response)
      throws Exception {
    HttpRequest req = new HttpRequest(REQUEST_URL).setMethod("POST")
        .setPostBody(REQUEST_BODY.getBytes("UTF-8"))
        .setAuthType(authType);
    when(fetcherFactory.fetch(req)).thenReturn(new HttpResponse(response));
    when(request.getParameter(MakeRequestHandler.METHOD_PARAM)).thenReturn("POST");
    when(request.getParameter(MakeRequestHandler.POST_DATA_PARAM))
        .thenReturn(REQUEST_BODY);
  }

  private JSONObject extractJsonFromResponse() throws JSONException {
    String body = recorder.getResponseAsString();
    assertStartsWith(MakeRequestHandler.UNPARSEABLE_CRUFT, body);
    body = body.substring(MakeRequestHandler.UNPARSEABLE_CRUFT.length());
    return new JSONObject(body).getJSONObject(REQUEST_URL.toString());
  }

  @Before
  public void setUp() {
    when(request.getMethod()).thenReturn("POST");
    when(request.getParameter(MakeRequestHandler.URL_PARAM))
        .thenReturn(REQUEST_URL.toString());
  }

  @Test
  public void testGetRequest() throws Exception {
    expectGetAndReturnBody(RESPONSE_BODY);

    handler.fetch(request, recorder);

    JSONObject results = extractJsonFromResponse();
    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testExplicitHeaders() throws Exception {
    String headerString = "X-Foo=bar&X-Bar=baz%20foo";

    HttpRequest expected = new HttpRequest(REQUEST_URL)
        .addHeader("X-Foo", "bar")
        .addHeader("X-Bar", "baz foo");
    when(fetcherFactory.fetch(expected)).thenReturn(new HttpResponse(RESPONSE_BODY));
    when(request.getParameter(MakeRequestHandler.HEADERS_PARAM)).thenReturn(headerString);

    handler.fetch(request, recorder);

    JSONObject results = extractJsonFromResponse();
    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testPostRequest() throws Exception {
    when(request.getParameter(MakeRequestHandler.METHOD_PARAM)).thenReturn("POST");
    expectPostAndReturnBody(REQUEST_BODY, RESPONSE_BODY);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testFetchContentTypeFeed() throws Exception {
    String entryTitle = "Feed title";
    String entryLink = "http://example.org/entry/0/1";
    String entrySummary = "This is the summary";
    String rss = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                 "<rss version=\"2.0\"><channel>" +
                 "<title>dummy</title>" +
                 "<link>http://example.org/</link>" +
                 "<item>" +
                 "<title>" + entryTitle + "</title>" +
                 "<link>" + entryLink + "</link>" +
                 "<description>" + entrySummary + "</description>" +
                 "</item>" +
                 "</channel></rss>";

    expectGetAndReturnBody(rss);
    when(request.getParameter(MakeRequestHandler.CONTENT_TYPE_PARAM)).thenReturn("FEED");

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    JSONObject feed = new JSONObject(results.getString("body"));
    JSONObject entry = feed.getJSONArray("Entry").getJSONObject(0);

    assertEquals(entryTitle, entry.getString("Title"));
    assertEquals(entryLink, entry.getString("Link"));
    assertNull("getSummaries has the wrong default value (should be false).",
        entry.optString("Summary", null));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testFetchFeedWithParameters() throws Exception {
    String entryTitle = "Feed title";
    String entryLink = "http://example.org/entry/0/1";
    String entrySummary = "This is the summary";
    String rss = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                 "<rss version=\"2.0\"><channel>" +
                 "<title>dummy</title>" +
                 "<link>http://example.org/</link>" +
                 "<item>" +
                 "<title>" + entryTitle + "</title>" +
                 "<link>" + entryLink + "</link>" +
                 "<description>" + entrySummary + "</description>" +
                 "</item>" +
                 "<item>" +
                 "<title>" + entryTitle + "</title>" +
                 "<link>" + entryLink + "</link>" +
                 "<description>" + entrySummary + "</description>" +
                 "</item>" +
                 "<item>" +
                 "<title>" + entryTitle + "</title>" +
                 "<link>" + entryLink + "</link>" +
                 "<description>" + entrySummary + "</description>" +
                 "</item>" +
                 "</channel></rss>";

    expectGetAndReturnBody(rss);
    when(request.getParameter(MakeRequestHandler.GET_SUMMARIES_PARAM)).thenReturn("true");
    when(request.getParameter(MakeRequestHandler.NUM_ENTRIES_PARAM)).thenReturn("2");
    when(request.getParameter(MakeRequestHandler.CONTENT_TYPE_PARAM)).thenReturn("FEED");

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    JSONObject feed = new JSONObject(results.getString("body"));
    JSONArray feeds = feed.getJSONArray("Entry");

    assertEquals("numEntries not parsed correctly.", 2, feeds.length());

    JSONObject entry = feeds.getJSONObject(1);
    assertEquals(entryTitle, entry.getString("Title"));
    assertEquals(entryLink, entry.getString("Link"));
    assertTrue("getSummaries not parsed correctly.", entry.has("Summary"));
    assertEquals(entrySummary, entry.getString("Summary"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testFetchEmptyDocument() throws Exception {
    expectGetAndReturnBody("");

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals("", results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  private void expectParameters(HttpServletRequest request, String... params) {
    final List<String> v = Lists.newArrayList(params);

    when(request.getParameterNames()).thenAnswer((a) -> Collections.enumeration(v));
  }

  @Test
  public void testSignedGetRequest() throws Exception {

    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId()))
        .thenReturn(DUMMY_TOKEN);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM))
        .thenReturn(AuthType.SIGNED.toString());
    HttpRequest expected = new HttpRequest(REQUEST_URL)
        .setAuthType(AuthType.SIGNED);
    when(fetcherFactory.fetch(expected))
        .thenReturn(new HttpResponse(RESPONSE_BODY));
    expectParameters(request, MakeRequestHandler.AUTHZ_PARAM);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testSignedPostRequest() throws Exception {
    // Doesn't actually sign since it returns the standard fetcher.
    // Signing tests are in SigningFetcherTest
    expectPostAndReturnBody(AuthType.SIGNED, REQUEST_BODY, RESPONSE_BODY);
    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId()))
        .thenReturn(DUMMY_TOKEN);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM))
        .thenReturn(AuthType.SIGNED.toString());
    expectParameters(request, MakeRequestHandler.METHOD_PARAM, MakeRequestHandler.POST_DATA_PARAM,
        MakeRequestHandler.AUTHZ_PARAM);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(RESPONSE_BODY, results.get("body"));
    assertFalse("A security token was returned when it was not requested.",
        results.has("st"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testChangeSecurityToken() throws Exception {
    // Doesn't actually sign since it returns the standard fetcher.
    // Signing tests are in SigningFetcherTest
    expectGetAndReturnBody(AuthType.SIGNED, RESPONSE_BODY);
    FakeGadgetToken authToken = new FakeGadgetToken().setUpdatedToken("updated");
    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId()))
        .thenReturn(authToken);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM))
        .thenReturn(AuthType.SIGNED.toString());
    expectParameters(request, MakeRequestHandler.AUTHZ_PARAM);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(RESPONSE_BODY, results.get("body"));
    assertEquals("updated", results.getString("st"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testDoOAuthRequest() throws Exception {
    // Doesn't actually do oauth dance since it returns the standard fetcher.
    // OAuth tests are in OAuthFetcherTest
    expectGetAndReturnBody(AuthType.OAUTH, RESPONSE_BODY);
    FakeGadgetToken authToken = new FakeGadgetToken().setUpdatedToken("updated");
    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId()))
        .thenReturn(authToken);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM))
        .thenReturn(AuthType.OAUTH.toString());
    // This isn't terribly accurate, but is close enough for this test.
    when(request.getParameterMap()).thenAnswer((a) -> Collections.EMPTY_MAP);
    expectParameters(request);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testInvalidSigningTypeTreatedAsNone() throws Exception {
    expectGetAndReturnBody(RESPONSE_BODY);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM)).thenReturn("garbage");

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(HttpResponse.SC_OK, results.getInt("rc"));
    assertEquals(RESPONSE_BODY, results.get("body"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testBadHttpResponseIsPropagated() throws Exception {
    HttpRequest internalRequest = new HttpRequest(REQUEST_URL);
    when(fetcherFactory.fetch(internalRequest)).thenReturn(HttpResponse.error());

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(HttpResponse.SC_INTERNAL_SERVER_ERROR, results.getInt("rc"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testBadSecurityTokenThrows() throws Exception {
    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId()))
        .thenReturn(null);
    when(request.getParameter(MakeRequestHandler.AUTHZ_PARAM))
        .thenReturn(AuthType.SIGNED.toString());

    try {
      handler.fetch(request, recorder);
      fail("Should have thrown");
    } catch (GadgetException e) {
      // good.
    }
  }

  @Test
  public void testMetadataCopied() throws Exception {
    HttpRequest internalRequest = new HttpRequest(REQUEST_URL);
    HttpResponse response = new HttpResponseBuilder()
        .setResponse("foo".getBytes("UTF-8"))
        .setMetadata("foo", RESPONSE_BODY)
        .create();

    when(fetcherFactory.fetch(internalRequest)).thenReturn(response);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();

    assertEquals(RESPONSE_BODY, results.getString("foo"));
    assertTrue(rewriter.responseWasRewritten());
  }

  @Test
  public void testSetCookiesReturned() throws Exception {
    HttpRequest internalRequest = new HttpRequest(REQUEST_URL);
    HttpResponse response = new HttpResponseBuilder()
        .setResponse("foo".getBytes("UTF-8"))
        .addHeader("Set-Cookie", "foo=bar; Secure")
        .addHeader("Set-Cookie", "name=value")
        .create();

    when(fetcherFactory.fetch(internalRequest)).thenReturn(response);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();
    JSONObject headers = results.getJSONObject("headers");
    assertNotNull(headers);
    JSONArray cookies = headers.getJSONArray("set-cookie");
    assertNotNull(cookies);
    assertEquals(2, cookies.length());
    assertEquals("foo=bar; Secure", cookies.get(0));
    assertEquals("name=value", cookies.get(1));
  }

  @Test
  public void testLocationReturned() throws Exception {
    HttpRequest internalRequest = new HttpRequest(REQUEST_URL);
    HttpResponse response = new HttpResponseBuilder()
        .setResponse("foo".getBytes("UTF-8"))
        .addHeader("Location", "somewhere else")
        .create();

    when(fetcherFactory.fetch(internalRequest)).thenReturn(response);

    handler.fetch(request, recorder);
    JSONObject results = extractJsonFromResponse();
    JSONObject headers = results.getJSONObject("headers");
    assertNotNull(headers);
    JSONArray locations = headers.getJSONArray("location");
    assertNotNull(locations);
    assertEquals(1, locations.length());
    assertEquals("somewhere else", locations.get(0));
  }
}
