/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.social.opensocial.service;

import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.social.core.util.BeanJsonConverter;
import org.apache.shindig.social.core.util.ContainerConf;
import org.apache.shindig.social.core.util.JsonContainerConf;
import org.apache.shindig.social.opensocial.spi.AppDataService;
import org.apache.shindig.social.opensocial.spi.DataCollection;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.SocialSpiException;
import org.apache.shindig.social.opensocial.spi.UserId;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AppDataHandlerTest {

  private BeanJsonConverter converter;

  private AppDataService appDataService;

  private AppDataHandler handler;

  private FakeGadgetToken token;

  private RestfulRequestItem request;


  private static final Set<UserId> JOHN_DOE = Collections.unmodifiableSet(newHashSet(new UserId(UserId.Type.userId, "john.doe")));


  @Before
  public void setUp() throws Exception {
    token = new FakeGadgetToken();
    converter = mock(BeanJsonConverter.class);
    appDataService = mock(AppDataService.class);
    ContainerConf containerConf = new JsonContainerConf();
    handler = new AppDataHandler(appDataService, containerConf);
  }

  private void setPath(String path) {
    Map<String, String> params = newHashMap();
    params.put("fields", null);
    this.setPathAndParams(path, params);
  }

  private void setPathAndParams(String path, Map<String, String> params) {
    setPathAndParams(path, params, null);
  }

  private void setPathAndParams(String path, Map<String, String> params, String postData) {
    request = new RestfulRequestItem(path, "GET", postData, token, converter);
    for (Map.Entry<String, String> entry : params.entrySet()) {
      request.setParameter(entry.getKey(), entry.getValue());
    }
  }

  private void assertHandleGetForGroup(GroupId.Type group) throws Exception {
    setPath("/activities/john.doe/@" + group.toString() + "/appId");

    DataCollection data = new DataCollection(null);
    when(appDataService.getPersonData(JOHN_DOE,
        new GroupId(group, null),
        "appId", newHashSet(), token)).thenReturn(ImmediateFuture.newInstance(data));

    assertEquals(data, handler.handleGet(request).get());
  }

  @Test
  public void testHandleGetAll() throws Exception {
    assertHandleGetForGroup(GroupId.Type.all);
  }

  @Test
  public void testHandleGetFriends() throws Exception {
    assertHandleGetForGroup(GroupId.Type.friends);
  }

  @Test
  public void testHandleGetSelf() throws Exception {
    assertHandleGetForGroup(GroupId.Type.self);
  }

  @Test
  public void testHandleGetPlural() throws Exception {
    setPath("/activities/john.doe,jane.doe/@self/appId");

    DataCollection data = new DataCollection(null);
    Set<UserId> userIdSet = newLinkedHashSet(JOHN_DOE);
    userIdSet.add(new UserId(UserId.Type.userId, "jane.doe"));
    when(appDataService.getPersonData(userIdSet,
        new GroupId(GroupId.Type.self, null),
        "appId", newHashSet(), token)).thenReturn(ImmediateFuture.newInstance(data));

    assertEquals(data, handler.handleGet(request).get());
  }

  @Test
  public void testHandleGetWithoutFields() throws Exception {
    Map<String, String> params = newHashMap();
    params.put("fields", "pandas");
    setPathAndParams("/appData/john.doe/@friends/appId", params);

    DataCollection data = new DataCollection(null);
    when(appDataService.getPersonData(JOHN_DOE,
        new GroupId(GroupId.Type.friends, null),
        "appId", newHashSet("pandas"), token)).thenReturn(ImmediateFuture.newInstance(data));

    assertEquals(data, handler.handleGet(request).get());
  }

  private void setupPostData() throws SocialSpiException {
    String jsonAppData = "{pandas: 'are fuzzy'}";

    Map<String, String> params = newHashMap();
    params.put("fields", "pandas");
    setPathAndParams("/appData/john.doe/@self/appId", params, jsonAppData);

    HashMap<String, String> values = newHashMap();
    when(converter.convertToObject(jsonAppData, HashMap.class)).thenReturn(values);

    when(appDataService.updatePersonData(JOHN_DOE.iterator().next(),
        new GroupId(GroupId.Type.self, null),
        "appId", newHashSet("pandas"), values, token))
        .thenReturn(ImmediateFuture.newInstance(null));
  }

  @Test
  public void testHandlePost() throws Exception {
    setupPostData();
    assertNull(handler.handlePost(request).get());
  }

  @Test
  public void testHandlePut() throws Exception {
    setupPostData();
    assertNull(handler.handlePut(request).get());
  }

  /**
   * Test that the handler correctly recognizes null keys in the data.
   * @throws Exception if the test fails
   */
  @Test
  public void testHandleNullPostDataKeys() throws Exception {
    String jsonAppData = "{pandas: 'are fuzzy'}";

    Map<String, String> params = newHashMap();
    params.put("fields", "pandas");
    setPathAndParams("/appData/john.doe/@self/appId", params, jsonAppData);

    HashMap<String, String> values = newHashMap();
    // create an invalid set of app data and inject
    values.put("Aokkey", "an ok key");
    values.put("", "an empty value");
    when(converter.convertToObject(jsonAppData, HashMap.class)).thenReturn(values);

    try {
      handler.handlePost(request).get();
      fail();
    } catch (SocialSpiException spi) {
      // was expecting an Exception
    }
  }
  /**
   * Test that the handler correctly recognizes invalid keys in the data.
   * @throws Exception if the test fails
   */
  @Test
  public void testHandleInvalidPostDataKeys() throws Exception {
    String jsonAppData = "{pandas: 'are fuzzy'}";

    Map<String, String> params = newHashMap();
    params.put("fields", "pandas");
    setPathAndParams("/appData/john.doe/@self/appId", params, jsonAppData);

    HashMap<String, String> values = newHashMap();
    // create an invalid set of app data and inject
    values.put("Aokkey", "an ok key");
    values.put("a bad key", "a good value");
    when(converter.convertToObject(jsonAppData, HashMap.class)).thenReturn(values);

    try {
      handler.handlePost(request).get();
      fail();
    } catch (SocialSpiException spi) {
      // was expecting an Exception
    }
  }


  @Test
  public void testHandleDelete() throws Exception {
    Map<String, String> params = newHashMap();
    params.put("fields", "pandas");
    setPathAndParams("/appData/john.doe/@self/appId", params);

    when(appDataService.deletePersonData(JOHN_DOE.iterator().next(),
        new GroupId(GroupId.Type.self, null),
        "appId", newHashSet("pandas"), token))
        .thenReturn(ImmediateFuture.newInstance(null));

    assertNull(handler.handleDelete(request).get());
  }
}
