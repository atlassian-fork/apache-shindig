/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.social.opensocial.service;

import com.google.common.collect.Lists;
import com.google.inject.Guice;
import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.social.core.util.BeanJsonConverter;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.PersonService;
import org.apache.shindig.social.opensocial.spi.UserId;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RpcRequestItemTest {

  private static final FakeGadgetToken FAKE_TOKEN = new FakeGadgetToken();

  private RpcRequestItem request;

  private JSONObject baseRpc;

  @Before
  public void setUp() throws Exception {
    baseRpc = new JSONObject(
        "{method:people.get,id:id,params:{"
            + "userId:john.doe,"
            + "groupId:@self,"
            + "fields:[huey,dewey,louie]"
            + "}}");
    request = new RpcRequestItem(baseRpc, FAKE_TOKEN, new BeanJsonConverter(Guice.createInjector()));
  }

  @Test
  public void testParseMethod() throws Exception {
    assertEquals("people", request.getService());
    assertEquals(Lists.newArrayList("huey", "dewey", "louie"), request.getListParameter("fields"));

    // Try it without any params
    JSONObject noParams = new JSONObject(baseRpc.toString());
    noParams.remove("params");
    request = new RpcRequestItem(noParams, FAKE_TOKEN, null);

    assertEquals("people", request.getService());
    assertEquals(emptyList(), request.getListParameter("fields"));
  }

  @Test
  public void testGetAppId() {
    request.setParameter("appId", "100");
    assertEquals("100", request.getAppId());

    request.setParameter("appId", "@app");
    assertEquals(FAKE_TOKEN.getAppId(), request.getAppId());
  }

  @Test
  public void testGetUser() {
    request.setParameter("userId", "@owner");
    assertEquals(UserId.Type.owner, request.getUsers().iterator().next().getType());
  }

  @Test
  public void testGetGroup() {
    request.setParameter("groupId", "@self");
    assertEquals(GroupId.Type.self, request.getGroup().getType());
  }

  @Test
  public void testStartIndex() {
    request.setParameter("startIndex", null);
    assertEquals(0, request.getStartIndex());

    request.setParameter("startIndex", "5");
    assertEquals(5, request.getStartIndex());
  }

  @Test
  public void testCount() {
    request.setParameter("count", null);
    assertEquals(20, request.getCount());

    request.setParameter("count", "5");
    assertEquals(5, request.getCount());
  }

  @Test
  public void testSortBy() {
    request.setParameter("sortBy", null);
    assertEquals("topFriends", request.getSortBy());

    request.setParameter("sortBy", "name");
    assertEquals("name", request.getSortBy());
  }

  @Test
  public void testSortOrder() {
    request.setParameter("sortOrder", null);
    assertEquals(PersonService.SortOrder.ascending, request.getSortOrder());

    request.setParameter("sortOrder", "descending");
    assertEquals(PersonService.SortOrder.descending, request.getSortOrder());
  }

  @Test
  public void testFilterBy() {
    request.setParameter("filterBy", null);
    assertNull(request.getFilterBy());

    request.setParameter("filterBy", "hasApp");
    assertEquals("hasApp", request.getFilterBy());
  }

  @Test
  public void testFilterOperation() {
    request.setParameter("filterOp", null);
    assertEquals(PersonService.FilterOperation.contains, request.getFilterOperation());

    request.setParameter("filterOp", "equals");
    assertEquals(PersonService.FilterOperation.equals, request.getFilterOperation());
  }

  @Test
  public void testFilterValue() {
    request.setParameter("filterValue", null);
    assertEquals("", request.getFilterValue());

    request.setParameter("filterValue", "cassie");
    assertEquals("cassie", request.getFilterValue());
  }

  @Test
  public void testFields() {
    request.setListParameter("fields", emptyList());
    assertEquals(newHashSet(), request.getFields());

    request.setListParameter("fields", Lists.newArrayList("happy", "sad", "grumpy"));
    assertEquals(newHashSet("happy", "sad", "grumpy"), request.getFields());
  }
  
  public static class InputData {
    String name;
    int id;
    
    public void setName(String name) {
      this.name = name;
    }
    
    public void setId(int id) {
      this.id = id;
    }
  }

  @Test
  public void testGetTypedParameter() throws Exception {
    JSONObject obj = new JSONObject();
    obj.put("name", "Bob");
    obj.put("id", "1234");
    
    request.setJsonParameter("tp", obj);
    
    InputData input = request.getTypedParameter("tp", InputData.class);
    assertEquals("Bob", input.name);
    assertEquals(1234, input.id);
  }

  @Test
  public void testGetTypedParameters() {
    request.setParameter("name", "Bob");
    request.setParameter("id", "1234");
    
    InputData input = request.getTypedParameters(InputData.class);
    assertEquals("Bob", input.name);
    assertEquals(1234, input.id);
  }
}
