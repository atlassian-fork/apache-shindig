/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.social.opensocial.service;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.social.core.util.BeanJsonConverter;
import org.apache.shindig.social.core.util.BeanXStreamAtomConverter;
import org.apache.shindig.social.core.util.BeanXStreamConverter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.concurrent.Future;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 *
 */
public class JsonRpcServletTest {

  private static final FakeGadgetToken FAKE_GADGET_TOKEN = new FakeGadgetToken()
      .setOwnerId("john.doe").setViewerId("john.doe");

  private HttpServletRequest req;
  private HttpServletResponse res;
  private JsonRpcServlet servlet;

  private PersonHandler peopleHandler;
  private ActivityHandler activityHandler;
  private AppDataHandler appDataHandler;

  private BeanJsonConverter jsonConverter;

  @Before
  public void setUp() throws Exception {
    servlet = new JsonRpcServlet();
    req = mock(HttpServletRequest.class);
    res = mock(HttpServletResponse.class);
    jsonConverter = mock(BeanJsonConverter.class);
    BeanXStreamConverter xmlConverter = mock(BeanXStreamConverter.class);
    BeanXStreamAtomConverter atomConverter = mock(BeanXStreamAtomConverter.class);

    peopleHandler = mock(PersonHandler.class);
    activityHandler = mock(ActivityHandler.class);
    appDataHandler = mock(AppDataHandler.class);

    HandlerDispatcher dispatcher = new StandardHandlerDispatcher(constant(peopleHandler),
        constant(activityHandler), constant(appDataHandler));
    servlet.setHandlerDispatcher(dispatcher);

    servlet.setBeanConverters(jsonConverter, xmlConverter, atomConverter);
  }

  // TODO: replace with Providers.of() when Guice version is upgraded
  private static <T> Provider<T> constant(final T value) {
    return () -> value;
  }

  @Test
  public void testPeopleMethodRecognition() throws Exception {
    verifyHandlerWasFoundForMethod("{method:people.get,id:id,params:{userId:5,groupId:@self}}",
        peopleHandler);
  }

  @Test
  public void testActivitiesMethodRecognition() throws Exception {
    verifyHandlerWasFoundForMethod("{method:activities.get,id:id,params:{userId:5,groupId:@self}}",
        activityHandler);
  }

  @Test
  public void testAppDataMethodRecognition() throws Exception {
    verifyHandlerWasFoundForMethod("{method:appdata.get,id:id,params:{userId:5,groupId:@self}}",
        appDataHandler);
  }

  @Test
  public void testInvalidService() throws Exception {
    String json = "{method:junk.get,id:id,params:{userId:5,groupId:@self}}";
    setupRequest(json);

    JSONObject err = new JSONObject(
        "{id:id,error:{message:'notImplemented: The service junk is not implemented',code:501}}");

    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);

    servlet.service(req, res);

    verify(writerMock).write(err.toString());
  }


  /**
   * Tests a data handler that returns a failed Future.
   * @throws Exception on failure
   */
  @Test
  public void testFailedRequest() throws Exception {
    setupRequest("{id:id,method:appdata.get}");
    when(appDataHandler.handleItem(isA(RpcRequestItem.class))).thenReturn(
        ImmediateFuture.errorInstance(new RuntimeException("FAILED")));

    JSONObject err = new JSONObject(
        "{id:id,error:{message:'internalError: FAILED',code:500}}");

    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);

    servlet.service(req, res);

    verify(writerMock).write(err.toString());
  }

  private void verifyHandlerWasFoundForMethod(String json, DataRequestHandler handler)
      throws Exception {
    setupRequest(json);

    String resultObject = "my lovely json";

    when(handler.handleItem(isA(RequestItem.class)))
            .then((Answer<Future<?>>) invocationOnMock -> ImmediateFuture.newInstance(resultObject));

    when(jsonConverter.convertToJson(resultObject))
        .thenReturn(new JSONObject(ImmutableMap.of("foo", "bar")));

    JSONObject result = new JSONObject();
    result.put("id", "id");
    result.put("data", ImmutableMap.of("foo", "bar"));
    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);

    servlet.service(req, res);

    verify(writerMock).write(result.toString());
  }

  @Test
  public void testBasicBatch() throws Exception {
    String batchJson =
        "[{method:people.get,id:'1'},{method:activities.get,id:'2'}]";
    setupRequest(batchJson);

    String resultObject = "my lovely json";
    Future<?> responseItemFuture = ImmediateFuture.newInstance(resultObject);

    when(peopleHandler.handleItem(isA(RequestItem.class)))
            .then((Answer<Future<?>>) invocationOnMock -> responseItemFuture);
    when(activityHandler.handleItem(isA(RequestItem.class)))
            .then((Answer<Future<?>>) invocationOnMock -> responseItemFuture);
    when(jsonConverter.convertToJson(resultObject))
        .thenReturn(new JSONObject(ImmutableMap.of("foo", "bar")));

    JSONArray result = new JSONArray("[{id:'1',data:{foo:'bar'}}," + "{id:'2',data:{foo:'bar'}}]");
    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);

    servlet.service(req, res);

    verify(writerMock).write(result.toString());
  }

  @Test
  public void testGetExecution() throws Exception {
    when(req.getParameterMap()).thenReturn(
        ImmutableMap.of("method", new String[]{"people.get"}, "id", new String[]{"1"}));
    when(req.getMethod()).thenReturn("GET");
    when(req.getAttribute(isA(String.class))).thenReturn(FAKE_GADGET_TOKEN);
    when(req.getCharacterEncoding()).thenReturn("UTF-8");

    res.setContentType("application/json");
    res.setCharacterEncoding("UTF-8");

    String resultObject = "my lovely json";

    Future<?> responseItemFuture = ImmediateFuture.newInstance(resultObject);
    when(peopleHandler.handleItem(isA(RequestItem.class))).then((Answer<Future<?>>) invoke -> responseItemFuture);

    when(jsonConverter.convertToJson(resultObject))
        .thenReturn(new JSONObject(ImmutableMap.of("foo", "bar")));

    JSONObject result = new JSONObject("{id:'1',data:{foo:'bar'}}");
    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);


    servlet.service(req, res);

    verify(writerMock).write(result.toString());
  }

  private void setupRequest(String json) throws IOException {
    final InputStream in = new ByteArrayInputStream(json.getBytes());
    ServletInputStream stream = new ServletInputStream() {
      @Override
      public int read() throws IOException {
        return in.read();
      }
    };

    when(req.getInputStream()).thenReturn(stream);
    when(req.getMethod()).thenReturn("POST");
    when(req.getAttribute(isA(String.class))).thenReturn(FAKE_GADGET_TOKEN);
    when(req.getCharacterEncoding()).thenReturn("UTF-8");
    res.setCharacterEncoding("UTF-8");
    res.setContentType("application/json");
  }

}
