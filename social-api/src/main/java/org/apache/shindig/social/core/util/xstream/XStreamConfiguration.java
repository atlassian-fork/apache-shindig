package org.apache.shindig.social.core.util.xstream;

import com.google.inject.ImplementedBy;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import com.thoughtworks.xstream.mapper.Mapper;

import java.util.Map;

/**
 * The configuration for the XStream converter, this class encapsulates the
 * lists and maps that define the how xstream converts the model.
 */
@ImplementedBy(XStream081Configuration.class)
public interface XStreamConfiguration {

  enum ConverterSet {
    MAP,
    COLLECTION,
    DEFAULT
  }

  class ConverterConfig {

    public InterfaceClassMapper mapper;
    public XStream xstream;

    ConverterConfig(InterfaceClassMapper mapper, XStream xstream) {
      this.mapper = mapper;
      this.xstream = xstream;
    }
  }

  /**
   * Generate the converter config.
   * 
   * @param c
   *          which converter set.
   * @param rp
   *          an XStream reflection provider.
   * @param dmapper
   *          the XStream mapper.
   * @param driver
   *          the XStream driver
   * @param writerStack
   *          a hirachical stack recorder.
   * @return the converter config, used for serialization.
   */
  ConverterConfig getConverterConfig(ConverterSet c, ReflectionProvider rp,
      Mapper dmapper, HierarchicalStreamDriver driver, WriterStack writerStack);

  /**
   * @return get the namespace mappings used by the driver.
   */
  Map<String, NamespaceSet> getNameSpaces();
}
